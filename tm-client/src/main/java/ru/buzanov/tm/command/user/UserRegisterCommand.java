package ru.buzanov.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.RoleType;
import ru.buzanov.tm.endpoint.User;

public class UserRegisterCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "register";
    }

    @NotNull
    @Override
    public String description() {
        return "User registration";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[REGISTRATION]");
        terminalService.printLineG("[ENTER LOGIN]");
        @NotNull String stringBuf = terminalService.readLine();
        if (stringBuf.isEmpty()) {
            terminalService.printLine("Login can't be empty");
            return;
        }
        if (userService.isLoginExist(stringBuf)) {
            terminalService.printLine("This login already exist");
            return;
        }
        @NotNull final User user = new User();
        user.setLogin(stringBuf);
        terminalService.printLineG("[ENTER PASS]");
        stringBuf = terminalService.readLine();
        if (stringBuf.isEmpty() || stringBuf.length() < 6) {
            terminalService.printLine("Pass can't be empty and less then 6 symbols");
            return;
        }
        terminalService.printLineG("[REPEAT PASS]");

        if (!stringBuf.equals(terminalService.readLine())) {
            terminalService.printLine("Invalid pass");
            return;
        }
        user.setPasswordHash(DigestUtils.md5Hex(stringBuf));
        terminalService.printLineG("[ENTER YOUR NAME]");
        stringBuf = terminalService.readLine();
        user.setName(stringBuf);
        user.setRoleType(RoleType.ПОЛЬЗОВАТЕЛЬ);
        userService.registryUser(user);
        terminalService.printLineG("[Hello, ", user.getName(), ", now type auth to log in]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
