package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;

public class UserLogOutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "log-out";
    }

    @NotNull
    @Override
    public String description() {
        return "User log-out";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.setCurrentUser(null);
        sessionService.killSession(serviceLocator.getCurrentSession());
        serviceLocator.setCurrentSession(null);
        terminalService.printLineG("[YOU ARE UNAUTHORIZED NOW]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
