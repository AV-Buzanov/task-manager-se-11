package ru.buzanov.tm.command.task;


import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Session;
import ru.buzanov.tm.endpoint.Task;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TaskSortListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-list-sorted";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks sorted";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[SORTED TASK LIST]");
        @NotNull final Session session = serviceLocator.getCurrentSession();
        @NotNull List<Task> list = (List<Task>) taskService.findAllT(session);
        terminalService.printLineG("[SORT BY]");
        terminalService.printLine("1 : By creation");
        terminalService.printLine("2 : By name");
        terminalService.printLine("3 : By start date");
        terminalService.printLine("4 : By end date");
        terminalService.printLine("5 : By status");
        @NotNull final String comp = terminalService.readLine();
        terminalService.printLineG("[DIRECTION]");
        terminalService.printLine("1 : Rising");
        terminalService.printLine("2 : Falling");
        boolean dir = true;
        if ("2".equals(terminalService.readLine()))
            dir = false;

        switch (comp) {
            case ("1"):
                if (dir)
                    break;
                else
                    Collections.reverse(Objects.requireNonNull(list));
                break;
            case ("2"):
                list.sort(terminalService.getNameComparator(dir));
                break;
            case ("3"):
                list.sort(terminalService.getStartDateComparator(dir));
                break;
            case ("4"):
                list.sort(terminalService.getEndDateComparator(dir));
                break;
            case ("5"):
                list.sort(terminalService.getStatusComparator(dir));
                break;
            default:
                break;
        }

        for (@NotNull final Task task : list) {
            terminalService.printWBS(task);
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
