package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.*;
import ru.buzanov.tm.util.TerminalService;

import java.lang.Exception;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {
    @NotNull
    private final ITerminalService terminalService = new TerminalService();
    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>();
    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.buzanov.tm").getSubTypesOf(AbstractCommand.class);
    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
    @NotNull
    private final UserEndpoint userEndPoint = new UserEndpointService().getUserEndpointPort();
    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpointService().getAdminUserEndpointPort();
    @Nullable
    private Session currentSession;
    @Nullable
    private User currentUser;

    @SneakyThrows
    private void init() {
        for (Class aClass : classes) {
            registryCommand((AbstractCommand) aClass.newInstance());
        }
    }

    private void registryCommand(@NotNull final AbstractCommand command) {
        if (command.command().isEmpty() || command.description().isEmpty())
            return;
        command.setServiceLocator(this);
        commands.put(command.command(), command);
    }

    public void start() {
        init();

        @NotNull String command = "";

        terminalService.printLineG("***WELCOME TO TASK MANAGER***");
        terminalService.printLine("Type help to see command list.");
        while (!commands.get("exit").command().equals(command)) {
            try {
                command = terminalService.readLine();
                if (!command.isEmpty()) {

                    if (getCurrentSession() == null && commands.get(command).isSecure())
                        terminalService.printLineR("Authorise, please");
                    else {
                        if (getCurrentUser() != null && !commands.get(command).isRoleAllow(getCurrentUser().getRoleType()))
                            terminalService.printLineR("This command is not allow for you");
                        else
                            commands.get(command).execute();
                    }
                }

            } catch (Exception | ru.buzanov.tm.endpoint.Exception e) {
                terminalService.printLineR(e.getMessage());
            }
        }
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }
}