package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractEntity {
    private String userId;
    private Date createDate = new Date(System.currentTimeMillis());
    private String signature;
}
