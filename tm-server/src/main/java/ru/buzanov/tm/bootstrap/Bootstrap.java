package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.IProjectRepository;
import ru.buzanov.tm.api.repository.ISessionRepository;
import ru.buzanov.tm.api.repository.ITaskRepository;
import ru.buzanov.tm.api.repository.IUserRepository;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.endpoint.*;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.SessionRepository;
import ru.buzanov.tm.repository.TaskRepository;
import ru.buzanov.tm.repository.UserRepository;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.SessionService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;

import javax.xml.ws.Endpoint;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {
    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull private final IUserRepository userRepository = new UserRepository();
    @NotNull private final ISessionRepository sessionRepository = new SessionRepository();
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull private final IUserService userService = new UserService(userRepository);
    @NotNull private final ISessionService sessionService = new SessionService(sessionRepository);
    @NotNull private final String adress = "http://172.18.128.106:8080/";
    @SneakyThrows
    private void init() {

        registryUser(new User("UserName", "user", DigestUtils.md5Hex("123456"), RoleType.USER));
        registryUser(new User("AdminName", "admin", DigestUtils.md5Hex("123456"), RoleType.ADMIN));
    }

    private void registryUser(@NotNull final User user) {
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            return;
        }
        if (user.getPasswordHash() == null || user.getPasswordHash().isEmpty()) {
            return;
        }
        if (user.getRoleType() == null) {
            return;
        }
        userService.load(user);
    }

    public void start() {
        init();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Endpoint.publish(adress+"ProjectEndpoint?wsdl", new ProjectEndpoint(this));
        Endpoint.publish(adress+"TaskEndpoint?wsdl", new TaskEndpoint(this));
        Endpoint.publish(adress+"AdminUserEndpoint?wsdl", new AdminUserEndpoint(this));
        Endpoint.publish(adress+"SessionEndpoint?wsdl", new SessionEndpoint(this));
        Endpoint.publish(adress+"UserEndpoint?wsdl", new UserEndpoint(this));

        System.out.println("Task manager server is running.");
        System.out.println(adress+"ProjectEndpoint?wsdl");
        System.out.println(adress+"TaskEndpoint?wsdl");
        System.out.println(adress+"AdminUserEndpoint?wsdl");
        System.out.println(adress+"SessionEndpoint?wsdl");
        System.out.println(adress+"UserEndpoint?wsdl");

        while (true) {
            try {
                if ("exit".equals(reader.readLine()))
                    System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}