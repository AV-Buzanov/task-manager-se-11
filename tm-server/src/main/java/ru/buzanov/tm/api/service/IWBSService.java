package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IWBSService<T> extends IService<T> {

    @NotNull Collection<T> findAll(@Nullable final String userId);

    @Nullable T findOne(@Nullable final String userId, @Nullable final String id);

    boolean isNameExist(@Nullable final String userId, @Nullable final String name);

    T setName(@Nullable final String name, @Nullable final T entity) throws Exception;

    @NotNull String getList();

    @Nullable T load(@Nullable final String userId, @Nullable final T entity);

    void load(@Nullable final String userId, final List<T> list);

    @Nullable String getList(@Nullable final String userId);

    @Nullable String getIdByCount(final int count);

    @Nullable String getIdByCount(@Nullable final String userId, final int count);

    void merge(@Nullable final String userId, @Nullable final String id, @Nullable final T project);

    @Nullable T remove(@Nullable final String userId, @Nullable final String id);

    void removeAll(@Nullable final String userId);

    @NotNull Collection<T> findByDescription(@Nullable final String userId, @Nullable final String desc);

    @NotNull Collection<T> findByName(@Nullable final String userId, @Nullable final String name);

    @NotNull Comparator<T> getNameComparator(final boolean direction);

    @NotNull Comparator<T> getStartDateComparator(final boolean direction);

    @NotNull Comparator<T> getEndDateComparator(final boolean direction);

    @NotNull Comparator<T> getStatusComparator(final boolean direction);
}
