package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface IRepository<T> {
    @NotNull Collection<T> findAll();

    @Nullable T findOne(@NotNull final String id);

    void merge(@NotNull final String id, @NotNull final T entity);

    @Nullable T remove(@NotNull final String id);

    void removeAll();

    @Nullable T load(@NotNull final T entity);

    void load(final List<T> list);
}
