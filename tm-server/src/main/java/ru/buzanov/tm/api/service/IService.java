package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface IService<T> {

    @NotNull Collection<T> findAll();

    @Nullable T findOne(@Nullable final String id);

    void merge(@Nullable final String id, @Nullable final T entity);

    @Nullable T remove(@Nullable final String id);

    void removeAll();

    @Nullable T load(@Nullable final T entity);

    void load(final List<T> list);
}
