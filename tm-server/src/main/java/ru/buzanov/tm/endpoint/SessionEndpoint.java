package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.constant.HashConst;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint {
    public SessionEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public Session getSession(String login, String passwordHash) throws Exception {
        if (login == null || login.isEmpty() || passwordHash == null || passwordHash.isEmpty())
            throw new Exception("empty login or password");
        if (!userService.isLoginExist(login))
            throw new Exception("Login doesn't exist.");
        if (!userService.isPassCorrect(login, passwordHash))
            throw new Exception("Invalid password.");
        Session session = new Session();
        session.setUserId(userService.findByLogin(login).getId());
        String signature = SignatureUtil.sign(session, HashConst.SALT, HashConst.CYCLE);
        session.setSignature(signature);
        sessionService.load(session);
        return session;
    }

    @WebMethod
    public void killSession(@Nullable Session session) throws Exception {
        auth(session);
        sessionService.remove(session.getId());
    }
}
