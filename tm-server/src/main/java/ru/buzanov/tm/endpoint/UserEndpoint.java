package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint {
    public UserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public User findOne(@Nullable Session session) throws Exception {
        auth(session);
        return userService.findOne(session.getUserId());
    }

    @WebMethod
    public void merge(@Nullable Session session, @Nullable User user) throws Exception {
        auth(session);
        if (user.getRoleType() != null && !userService.findOne(session.getUserId()).equals(user.getRoleType()))
            throw new Exception("You can't change Role.");
        if (user.getLogin() != null && userService.isLoginExist(user.getLogin()))
            throw new Exception("This login already exist.");
        userService.merge(session.getUserId(), user);
    }

    @WebMethod
    public boolean isLoginExist(@Nullable String login) {
        return userService.isLoginExist(login);
    }

    @WebMethod
    public void remove(@Nullable Session session) throws Exception {
        auth(session);
        userService.remove(session.getUserId());
    }

    @WebMethod
    public void registryUser(@Nullable User user) throws Exception {
        if (userService.isLoginExist(user.getLogin()))
            throw new Exception("This login already exist.");
        user.setRoleType(RoleType.USER);
        userService.load(user);
    }
}
