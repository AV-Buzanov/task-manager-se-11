package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.constant.HashConst;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.util.SignatureUtil;

public abstract class AbstractEndpoint {
    @NotNull
    final IProjectService projectService;
    @NotNull
    final ITaskService taskService;
    @NotNull
    final IUserService userService;
    @NotNull
    final ISessionService sessionService;

    public AbstractEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.projectService = serviceLocator.getProjectService();
        this.taskService = serviceLocator.getTaskService();
        this.userService = serviceLocator.getUserService();
        this.sessionService = serviceLocator.getSessionService();
    }

    protected void auth(@Nullable final Session session) throws Exception {
        if (session == null)
            throw new Exception("null session");
        if (System.currentTimeMillis() - session.getCreateDate().getTime() > HashConst.SESSION_LIFETIME)
            throw new Exception("Session time out.");
        @Nullable final String signature = session.getSignature();
        session.setSignature(null);
        @Nullable final String ourSignature = SignatureUtil.sign(session, HashConst.SALT, HashConst.CYCLE);
        if (signature == null || !signature.equals(ourSignature))
            throw new Exception("Invalid session signature.");
        if (sessionService.findOne(session.getId()) == null)
            throw new Exception("Session not found.");


        System.out.print(userService.findOne(session.getUserId()).getLogin()+"  ");
        System.out.print(Thread.currentThread().getStackTrace()[2].getClassName()+"  ");
        System.out.print(Thread.currentThread().getStackTrace()[2].getMethodName());
        System.out.println();
    }
}
